//
//  UIColor+AppColors.swift
//  MVVMApp
//
//  Created by varun bhargava on 28/04/19.
//  Copyright © 2019 Nagarro. All rights reserved.
//

import Foundation
import UIKit

extension UIColor {
    static func appToastMessageBackgroundColor() -> UIColor {
       return UIColor(red: 217/255.0, green: 96/255.0, blue: 86/255, alpha: 1.0)
    }
}
