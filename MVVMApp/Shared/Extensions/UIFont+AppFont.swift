//
//  UIFont+AppFont.swift
//  MVVMApp
//
//  Created by varun bhargava on 28/04/19.
//  Copyright © 2019 Nagarro. All rights reserved.
//

import Foundation
import UIKit

enum Font : String {
    var name : String { return rawValue }
    case Helvetica
}

extension UIFont {
   static func deliveryListCellFont() -> UIFont {
        let cellFont = UIFont(name: Font.Helvetica.name, size: 17)
        return cellFont ?? UIFont()
    }    
}
