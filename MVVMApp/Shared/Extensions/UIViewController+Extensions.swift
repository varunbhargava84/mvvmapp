//
//  UIViewController+Extensions.swift
//  MVVMApp
//
//  Created by varun bhargava on 28/04/19.
//  Copyright © 2019 Nagarro. All rights reserved.
//

import UIKit

extension UIViewController {
    func presentError(_ error: Error) {
        let alertController = UIAlertController(title: LocalizedString.error.value,
                                                message: error.localizedDescription,
                                                preferredStyle: .alert)
        alertController.addAction(.init(title: LocalizedString.ok.value, style: .default))
        self.present(alertController, animated: true)
    }
    func presentMessage(_ message: String) {
        let alertController = UIAlertController(title: LocalizedString.message.value,
                                                message: message,
                                                preferredStyle: .alert)
        alertController.addAction(.init(title: LocalizedString.ok.value, style: .default))
        self.present(alertController, animated: true)
    }
}
