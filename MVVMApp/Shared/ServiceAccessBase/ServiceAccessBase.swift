//
//  SeviceAccessBase.swift
//  MVVMApp
//
//  Created by varun bhargava on 25/04/19.
//  Copyright © 2019 Nagarro. All rights reserved.
//

import Foundation
import Alamofire

class ServiceAccessBase : ServiceAccessBaseProtocol {
    
    @discardableResult
    func performRequest<T: Decodable>(parameters: WebServiceParameters, decoder: JSONDecoder = JSONDecoder(), completion: @escaping (Result<T, Error>) -> Void) -> DataRequest {
        
        let dataRequest = AF.request(URL(string:parameters.serviceURL)!,
                                     method: parameters.method,
                                     parameters: parameters.queryParameters,
                                     encoding: parameters.encoding ?? URLEncoding.default,
                                     headers: parameters.headers)
        
        return  dataRequest.responseDecodable(decoder: decoder) { (response: DataResponse<T>) in
            completion(response.result)
        }
    }
}
