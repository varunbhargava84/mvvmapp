//
//  WebServiceParameters.swift
//  MVVMApp
//
//  Created by varun bhargava on 27/04/19.
//  Copyright © 2019 Nagarro. All rights reserved.
//

import Foundation
import Alamofire

struct WebServiceParameters {
    let serviceURL      :   String
    let method          :   HTTPMethod
    let queryParameters :   Parameters?
    let encoding        :   URLEncoding?
    let headers         :   HTTPHeaders?
    let interceptors    :   RequestInterceptor?
}




