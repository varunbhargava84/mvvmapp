//
//  PagingParameters.swift
//  MVVMApp
//
//  Created by varun bhargava on 01/05/19.
//  Copyright © 2019 Nagarro. All rights reserved.
//

import Foundation

public struct PagingParameters {
    let offset :   Int
    let limit  :   Int
}
