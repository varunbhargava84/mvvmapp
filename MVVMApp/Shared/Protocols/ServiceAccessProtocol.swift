//
//  ServiceAccessProtocol.swift
//  MVVMApp
//
//  Created by varun bhargava on 28/04/19.
//  Copyright © 2019 Nagarro. All rights reserved.
//

import Foundation

protocol ServiceAccessProtocol {
    func getDeliveries(pagingParameters: PagingParameters,
                              completion: @escaping (Result<[DeliveryServiceEntity], Error>) -> Void)
}
