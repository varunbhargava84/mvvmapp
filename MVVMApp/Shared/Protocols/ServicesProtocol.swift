//
//  ServicesProtocol.swift
//  MVVMApp
//
//  Created by varun bhargava on 28/04/19.
//  Copyright © 2019 Nagarro. All rights reserved.
//

import Foundation

protocol DeliveryServicesProtocol {
    
    func getDeliveryList(_ pagingParameters:PagingParameters, completion: @escaping([DeliveryListModel]?, Error?) -> Void )
}
