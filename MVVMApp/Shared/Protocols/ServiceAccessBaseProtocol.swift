//
//  ServiceAccessBaseProtocol.swift
//  MVVMApp
//
//  Created by varun bhargava on 28/04/19.
//  Copyright © 2019 Nagarro. All rights reserved.
//

import Foundation
import Alamofire

protocol ServiceAccessBaseProtocol {
    
    @discardableResult
    func performRequest<T: Decodable>(parameters: WebServiceParameters, decoder: JSONDecoder, completion: @escaping (Result<T, Error>) -> Void) -> DataRequest
}

extension ServiceAccessBaseProtocol {
    
    @discardableResult
    func performRequest<T: Decodable>(parameters: WebServiceParameters, decoder: JSONDecoder = JSONDecoder(), completion: @escaping (Result<T, Error>) -> Void) -> DataRequest {
        fatalError("Must Override")
    }
}
