//
//  DeliveryServiceAccess
//  MVVMApp
//
//  Created by varun bhargava on 25/04/19.
//  Copyright © 2019 Nagarro. All rights reserved.
//

import Foundation
import Alamofire

enum Parameter : String {
    var value : String { return rawValue }
    case offset
    case limit
}

class DeliveryServiceAccess: ServiceAccessProtocol {
    
    var serviceAccessBase:ServiceAccessBaseProtocol?
    
    init(_ serviceAccessBase:ServiceAccessBaseProtocol) {
        self.serviceAccessBase = serviceAccessBase
    }
    
    func getDeliveries(pagingParameters: PagingParameters,
                       completion: @escaping (Result<[DeliveryServiceEntity], Error>) -> Void) {
        let jsonDecoder = JSONDecoder()
        let queryParametrs = [Parameter.offset.value:pagingParameters.offset, Parameter.limit.value:pagingParameters.limit]
        let deliveryServiceeParameters = WebServiceParameters(serviceURL: ServiceLinks.deliveryServiceURL, method: .get, queryParameters:queryParametrs, encoding:nil, headers:nil, interceptors:nil)
        serviceAccessBase!.performRequest(parameters: deliveryServiceeParameters,
                                          decoder:jsonDecoder,
                                          completion:completion)
    }
}
