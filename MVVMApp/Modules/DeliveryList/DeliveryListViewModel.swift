//
//  DeliveryListViewModel.swift
//  MVVMApp
//
//  Created by varun bhargava on 25/04/19.
//  Copyright © 2019 Nagarro. All rights reserved.
//

import Foundation
import RxSwift

class DeliveryListViewModel: ViewModelBase {
    
    let kFetchLimit = 20
    var currentOffset : Int = 1
    public  var deliveryList : [DeliveryListModel] = []
    public  let bindableDeliveryList : PublishSubject<[DeliveryListModel]> = PublishSubject()
    public  let bindableShowTable : PublishSubject<Bool> = PublishSubject()
    public  let loading : ReplaySubject<Bool> = ReplaySubject.create(bufferSize: 2)
    public  let error : ReplaySubject<String> = ReplaySubject.create(bufferSize: 2)
    var deliveryService : DeliveryServicesProtocol?
    
    init(_ navigationController:UINavigationController?, _ deliveryService:DeliveryServicesProtocol) {
        super.init()
        self.navigationVC = navigationController
        self.deliveryService = deliveryService
        self.loading.onNext(true)
        let pagingParameters = PagingParameters(offset: currentOffset, limit: kFetchLimit)
        getDeliveryList(pagingParameters)
    }
    
    public func getDeliveryList (_ pagingParameters:PagingParameters) {
        deliveryService!.getDeliveryList(pagingParameters) { (result,error)  in
            self.loading.onNext(false)
            if let error = error {
                self.error.onNext(error.localizedDescription)
                if (self.deliveryList.count == 0) {
                    self.bindableShowTable.onNext(true)
                }
            }  else if let result = result {
                self.bindableShowTable.onNext(false)
                self.deliveryList.append(contentsOf: result)
                self.currentOffset = self.deliveryList.count + 1
                self.bindableDeliveryList.onNext(self.deliveryList)
            } else {
                self.error.onNext(LocalizedString.unknownError.value)
                if (self.deliveryList.count == 0) {
                    self.bindableShowTable.onNext(true)
                }
            }
        }
    }
    
    public func loadNextDeliveryListPage() {
        let pagingParameters = PagingParameters(offset: currentOffset, limit: kFetchLimit)
        getDeliveryList(pagingParameters)
    }
    
    public func navigateToDetailView (_ deliveryDetailsModel:DeliveryListModel) {
        let deliveryDetailsVC = DeliveryDetailsViewController()
        deliveryDetailsVC.deliveryListModel = deliveryDetailsModel
        navigationVC?.pushViewController(deliveryDetailsVC, animated: true)
    }
}
