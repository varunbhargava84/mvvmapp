//
//  DeliveryListViewController.swift
//  MVVMApp
//
//  Created by varun bhargava on 25/04/19.
//  Copyright © 2019 Nagarro. All rights reserved.
//

import UIKit
import RxSwift

struct Identifier {
    private init() {}
    static let deliveryListCell = "Identifier"
}

class DeliveryListViewController: UIViewController {
    
    var deliveryListView : DeliveryListView = DeliveryListView()
    var deliveryListViewModel : DeliveryListViewModel?
    let disposeBag = DisposeBag()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        initializeView()
        setupScrollDelegate()
        let appDelegate  = UIApplication.shared.delegate as! AppDelegate
        let navigationController = appDelegate.window?.rootViewController as! UINavigationController
        deliveryListViewModel = DeliveryListViewModel(navigationController, DeliveryServices(DeliveryServiceAccess(ServiceAccessBase())))
        setupBindings()
    }
    
    func initializeView() {
        title = LocalizedString.deliveryListTitle.value
        view.addSubview(deliveryListView)
        view.fullViewConstraints(deliveryListView)
        view.backgroundColor = .white
        deliveryListView.deliveryTableView!.isHidden = true
    }
    
    func setupScrollDelegate() {
        let scrollView =  deliveryListView.deliveryTableView! as UIScrollView
        scrollView.delegate =   self
    }
    
    func setupBindings() {
        errorBinder()
        tableViewVisibilityBinder()
        loadingStateBinder()
        deliveryListViewBinder()
        deliveryListViewCellBinder()
        itemSelected()
    }
    
    func errorBinder() {
        deliveryListViewModel!
            .error
            .observeOn(MainScheduler.instance)
            .subscribe(onNext: { (error) in
                print(error)
                self.presentMessage(error)
            }).disposed(by: disposeBag)
    }
    
    func tableViewVisibilityBinder() {
        deliveryListViewModel!
            .bindableShowTable
            .observeOn(MainScheduler.instance)
            .subscribe(onNext: { (state) in
                self.deliveryListView.deliveryTableView?.isHidden=state
            }).disposed(by: disposeBag)
    }
    
    func loadingStateBinder() {
        deliveryListViewModel!
            .loading
            .observeOn(MainScheduler.instance)
            .subscribe(onNext: { (state) in
                if(state) {
                    self.deliveryListView.activityIndicator?.startAnimating()
                } else {
                    self.deliveryListView.activityIndicator?.stopAnimating()
                }
            }).disposed(by: disposeBag)
    }
    
    func deliveryListViewBinder() {
        let listTableView = deliveryListView.deliveryTableView!
        listTableView.register(DeliveryTableViewCell.self, forCellReuseIdentifier: Identifier.deliveryListCell)
        deliveryListViewModel!
            .bindableDeliveryList
            .observeOn(MainScheduler.instance)
            .bind(to: listTableView.rx.items(cellIdentifier: Identifier.deliveryListCell, cellType: DeliveryTableViewCell.self)) { (row,data,cell) in
                cell.cellData = data
            }.disposed(by: disposeBag)
    }
    
    func deliveryListViewCellBinder() {
        deliveryListView.deliveryTableView!.rx.willDisplayCell
            .subscribe(onNext: ({ (cell,indexPath) in
                cell.alpha = 0
                let transform = CATransform3DTranslate(CATransform3DIdentity, 0, 0, 0)
                cell.layer.transform = transform
                UIView.animate(withDuration: 1, delay: 0, usingSpringWithDamping: 0.7, initialSpringVelocity: 0.5, options: .curveEaseOut, animations: {
                    cell.alpha = 1
                    cell.layer.transform = CATransform3DIdentity
                }, completion: nil)
            })).disposed(by: disposeBag)
    }
    
    func itemSelected() {
        let listTableView = deliveryListView.deliveryTableView!
        listTableView.rx.itemSelected
            .subscribe(onNext: { [weak self] indexPath in
                _ = listTableView.cellForRow(at: indexPath) as? DeliveryTableViewCell
                let detailViewModel = self!.deliveryListViewModel!.deliveryList[indexPath.row]
                self!.deliveryListViewModel!.navigateToDetailView (detailViewModel)
            }).disposed(by: disposeBag)
    }
}

extension DeliveryListViewController : UIScrollViewDelegate {
    
    func scrollViewDidEndDragging(_ scrollView: UIScrollView, willDecelerate decelerate: Bool) {
        let someOffset: CGFloat = 170.0
        let yOffset = CGFloat((scrollView.contentOffset.y))
        let tableHeight = (scrollView.frame.size.height)
        let contentsHeight = (scrollView.contentSize.height)
        if (yOffset + tableHeight) >= (contentsHeight - someOffset) {
            self.deliveryListViewModel?.loadNextDeliveryListPage()
        }
    }
}
