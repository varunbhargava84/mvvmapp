//
//  DeliveryListView.swift
//  MVVMApp
//
//  Created by varun bhargava on 25/04/19.
//  Copyright © 2019 Nagarro. All rights reserved.
//

import UIKit

class DeliveryListView: UIView {

    var deliveryTableView: UITableView?
    var activityIndicator: UIActivityIndicatorView?
    var refreshControlView: UIRefreshControl?
    
    override public init(frame: CGRect) {
        super.init(frame: frame)
        setupUI()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func setupUI() {
        self.setupTableView()
        self.setupActivityIndicator()
        self.setupPullToRefreshView()
        fullViewConstraints(deliveryTableView!)
    }
    
    func setupTableView() {
        deliveryTableView = UITableView(frame: self.frame)
        guard let deliveryTableView = deliveryTableView else { return }
        deliveryTableView.estimatedRowHeight = 120
        deliveryTableView.rowHeight = UITableView.automaticDimension
        self.addSubview(deliveryTableView)
    }
    
    func setupActivityIndicator() {
        activityIndicator   = UIActivityIndicatorView(frame: CGRect(x: 0, y: 0, width: 100, height: 44))
        guard let activityIndicator = activityIndicator else { return }
        activityIndicator.style = .gray
        activityIndicator.startAnimating()
        self.addSubview(activityIndicator)
        activityIndicator.center = self.center
    }
    
    func setupPullToRefreshView() {
        refreshControlView = UIRefreshControl(frame: CGRect(x: 0, y: 0, width: 100, height: 44))
        guard let refreshControlView = refreshControlView else { return }
        refreshControlView.tintColor = UIColor.red
        refreshControlView.attributedTitle = NSAttributedString(string: LocalizedString.pullToRefresh.value)
        refreshControlView.addTarget(self, action: #selector(pullToRefresh), for: .valueChanged)
        self.deliveryTableView?.addSubview(refreshControlView)
    }
    
    @objc func pullToRefresh() {
        
    }
}
