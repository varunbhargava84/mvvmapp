//
//  DeliveryTableViewCell.swift
//  MVVMApp
//
//  Created by varun bhargava on 25/04/19.
//  Copyright © 2019 Nagarro. All rights reserved.
//

import UIKit
import SDWebImage

class DeliveryTableViewCell: UITableViewCell {
    
    var imageIconView: UIImageView?
    var descriptionLabel: UILabel?
    
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        setupUI()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
    public var cellData : DeliveryListModel! {
        didSet {
            self.descriptionLabel!.text = String(cellData.id!) + " " + cellData.description! + LocalizedString.deliverMessageJoiner.value + cellData.address!
            self.imageIconView!.sd_setImage(with: URL.init(string: cellData.imageUrl ?? ""), completed: nil)
        }
    }
    
    func setupUI() {
        imageIconView = UIImageView()
        imageIconView?.contentMode = .scaleAspectFill
        imageIconView?.clipsToBounds = true
        self.addSubview(imageIconView!)
        
        descriptionLabel = UILabel()
        descriptionLabel?.font = UIFont.deliveryListCellFont()
        self.descriptionLabel?.numberOfLines = 0
        self.descriptionLabel?.translatesAutoresizingMaskIntoConstraints = false
        self.descriptionLabel?.text = nil
        self.descriptionLabel?.contentMode = .topLeft
        self.addSubview(descriptionLabel!)
        
        self.imageIconView?.anchor(top: topAnchor, left: leftAnchor, bottom: nil, right: nil, paddingTop: 10, paddingLeft: 10, paddingBottom: 0, paddingRight: 0, width: CGFloat(Constants.ImageDimension.kCellImageWidth), height: CGFloat(Constants.ImageDimension.kCellImageHeight), enableInsets: false)
        self.descriptionLabel?.anchor(top: self.imageIconView?.topAnchor, left: self.imageIconView?.rightAnchor, bottom: bottomAnchor, right: rightAnchor, paddingTop: 0, paddingLeft: 10, paddingBottom: 10, paddingRight: 10, width: 0, height: 0, enableInsets: false)

        self.descriptionLabel?.heightAnchor.constraint(greaterThanOrEqualToConstant: CGFloat(Constants.ImageDimension.kCellImageWidth)).isActive = true
    }
}
