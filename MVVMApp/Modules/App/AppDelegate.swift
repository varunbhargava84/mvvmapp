//
//  AppDelegate.swift
//  MVVMApp
//
//  Created by varun bhargava on 25/04/19.
//  Copyright © 2019 Nagarro. All rights reserved.
//

import UIKit

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

    var window: UIWindow?

    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        start()
        return true
    }
    
    public func start() {
        var navigationVC : UINavigationController? = nil
        self.window = UIWindow()
        let deliveryListVC = DeliveryListViewController()
        navigationVC = UINavigationController(rootViewController: deliveryListVC)
        self.window?.rootViewController = navigationVC
        self.window?.makeKeyAndVisible()
    }
}

