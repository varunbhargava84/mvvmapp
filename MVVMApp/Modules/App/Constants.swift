//
//  Constants.swift
//  MVVMApp
//
//  Created by varun bhargava on 28/04/19.
//  Copyright © 2019 Nagarro. All rights reserved.
//

import Foundation

struct Constants {
    
    private init() {}
    
    struct ImageDimension {
        private init() {}
        static let kCellImageHeight = 100.0
        static let kCellImageWidth = 100.0
    }
}
