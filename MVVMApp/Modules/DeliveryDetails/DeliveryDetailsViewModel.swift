//
//  DeliveryDetailsViewModel.swift
//  MVVMApp
//
//  Created by varun bhargava on 25/04/19.
//  Copyright © 2019 Nagarro. All rights reserved.
//

import Foundation
import RxSwift

class DeliveryDetailsViewModel: ViewModelBase {
    
    public  let bindableDeliveryDetails : ReplaySubject<DeliveryListModel> = ReplaySubject.create(bufferSize: 1)
    
    init(deliveryListModel : DeliveryListModel) {
        self.bindableDeliveryDetails.onNext(deliveryListModel)
    }
}
