//
//  DeliveryDetailsViewController.swift
//  MVVMApp
//
//  Created by varun bhargava on 25/04/19.
//  Copyright © 2019 Nagarro. All rights reserved.
//

import UIKit
import RxSwift
import RxCocoa

class DeliveryDetailsViewController: UIViewController {
    
    var deliveryDetailsView : DeliveryDetailsView = DeliveryDetailsView()
    var deliveryDetailsViewModel : DeliveryDetailsViewModel?
    let disposeBag = DisposeBag()
    public var deliveryListModel : DeliveryListModel?
    
    override func loadView() {
        let backgroundView: UIView = UIView(frame: UIScreen.main.bounds)
        backgroundView.backgroundColor = UIColor.white
        self.view = backgroundView
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        initialiseView()
        deliveryDetailsViewModel = DeliveryDetailsViewModel(deliveryListModel: deliveryListModel!)
        orderDetailViewBinder()
    }
    
    func initialiseView() {
        title = LocalizedString.deliveryDetailsTitle.value
        view.addSubview(deliveryDetailsView)
        self.view.fullViewConstraints(deliveryDetailsView)
    }
    
    func orderDetailViewBinder() {
        deliveryDetailsViewModel!
            .bindableDeliveryDetails
            .observeOn(MainScheduler.instance)
            .subscribe(onNext: { (viewModel) in
                self.deliveryDetailsView.mapViewData = viewModel
                self.deliveryDetailsView.descriptionViewData = viewModel
            }).disposed(by: disposeBag)
    }
}
