//
//  DeliveryServiceModel.swift
//  MVVMApp
//
//  Created by varun bhargava on 25/04/19.
//  Copyright © 2019 Nagarro. All rights reserved.
//

import Foundation

struct DeliveryServiceEntity : Codable {
    var id: Int?
    var description: String?
    var imageUrl: String?
    var location: Location?
}

struct Location: Codable {
    var address: String?
    var lat: Double?
    var lng: Double?
}

extension DeliveryServiceEntity {
    enum CodingKeys: String, CodingKey {
        case id
        case description
        case imageUrl
        case location
    }
}
