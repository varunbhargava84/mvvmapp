//
//  DeliveryLIst.swift
//  MVVMApp
//
//  Created by varun bhargava on 26/04/19.
//  Copyright © 2019 Nagarro. All rights reserved.
//

import Foundation

struct DeliveryListModel {
    var id: Int?
    var description: String?
    var imageUrl: String?
    var address: String?
    var lat: Double?
    var lng: Double?
}
