//
//  DeliveryServices.swift
//  MVVMApp
//
//  Created by varun bhargava on 25/04/19.
//  Copyright © 2019 Nagarro. All rights reserved.
//

import Foundation

class DeliveryServices: DeliveryServicesProtocol {
    
    var serviceAccess : ServiceAccessProtocol?
    
    init(_ serviceAccess:ServiceAccessProtocol) {
        self.serviceAccess = serviceAccess
    }
    
    func getDeliveryList(_ pagingParameters:PagingParameters, completion: @escaping ([DeliveryListModel]?, Error?) -> Void) {        
        serviceAccess!.getDeliveries(pagingParameters: pagingParameters) { (result) in
            switch result {
            case .success(let deliveryList):
                if (deliveryList.count == 0) {
                    completion(nil,self.createNoNewDataError())
                } else {
                    let deliveryListData = self.convertToDeliveryListModel(entityList: deliveryList)
                    completion(deliveryListData,nil)
                }
            case .failure(let error):
                completion(nil,error)
            }
        }
    }
    
    func convertToDeliveryListModel(entityList : [DeliveryServiceEntity]) -> [DeliveryListModel]{
        var deliveryListModel : [DeliveryListModel] = []
        for entity in entityList {
            let deliveryListModelElement = DeliveryListModel(id: entity.id,
                                                             description: entity.description,
                                                             imageUrl: entity.imageUrl,
                                                             address: entity.location?.address,
                                                             lat: entity.location?.lat,
                                                             lng: entity.location?.lng)
            deliveryListModel.append(deliveryListModelElement)
        }
        return deliveryListModel
    }
    
    func createNoNewDataError() -> NSError {
        return NSError(domain: LocalizedString.error.value, code: -1009, userInfo: [NSLocalizedDescriptionKey: LocalizedString.noNewData.value])
    }
}

