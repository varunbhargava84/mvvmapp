//
//  ServiceLinks.swift
//  MVVMApp
//
//  Created by varun bhargava on 29/04/19.
//  Copyright © 2019 Nagarro. All rights reserved.
//

import Foundation

struct ServiceLinks {
    static let deliveryServiceURL = "https://mock-api-mobile.dev.lalamove.com/deliveries"
}
