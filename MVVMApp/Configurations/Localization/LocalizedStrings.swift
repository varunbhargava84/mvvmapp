//
//  LocalizedStrings.swift
//  MVVMApp
//
//  Created by varun bhargava on 01/05/19.
//  Copyright © 2019 Nagarro. All rights reserved.
//

import Foundation

enum LocalizedString : String {
    var value: String {
        return NSLocalizedString(rawValue, comment: "")
    }
    
    case deliverMessageJoiner
    case deliveryListTitle
    case deliveryDetailsTitle
    case pullToRefresh
    case noNewData
    case unknownError
    case error
    case ok
    case message
}
