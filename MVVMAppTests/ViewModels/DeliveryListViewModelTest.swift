//
//  DeliveryListViewModelTest.swift
//  AlternateTests
//
//  Created by varun bhargava on 30/04/19.
//  Copyright © 2019 Nagarro. All rights reserved.
//

import XCTest

class MockDeliveryServicesWithData : DeliveryServicesProtocol {
    func getDeliveryList(_ pagingParameters: PagingParameters, completion: @escaping ([DeliveryListModel]?, Error?) -> Void) {
        let deliveryListModel = DeliveryListModel(id: 0, description: "Test data", imageUrl: nil, address: "Test address", lat: 10.0, lng: 10.0)
        let deliveryListModelList = [deliveryListModel]
        completion(deliveryListModelList,nil)
    }
}

class MockDeliveryServicesNoData : DeliveryServicesProtocol {
    func getDeliveryList(_ pagingParameters: PagingParameters, completion: @escaping ([DeliveryListModel]?, Error?) -> Void) {
        let noDataError = NSError(domain: LocalizedString.error.value, code: -1009, userInfo: [NSLocalizedDescriptionKey: LocalizedString.noNewData.value])
        completion(nil,noDataError)
    }
}

class DeliveryListViewModelTest: XCTestCase {
    
    var mockDeliveryViewModel : DeliveryListViewModel?
    
    override func setUp() {
        // Put setup code here. This method is called before the invocation of each test method in the class.
    }

    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
    }

    func testViewModelWithServiceSuccess() {
        mockDeliveryViewModel = DeliveryListViewModel(nil,MockDeliveryServicesWithData())
        mockDeliveryViewModel!.getDeliveryList(PagingParameters(offset: 1, limit: 20))
        XCTAssertEqual((mockDeliveryViewModel!.deliveryList.last)?.id!,0)
    }
    
    func testViewModelWithServicesNoData() {
        mockDeliveryViewModel = DeliveryListViewModel(nil,MockDeliveryServicesNoData())
        mockDeliveryViewModel!.getDeliveryList(PagingParameters(offset: 1, limit: 20))
        mockDeliveryViewModel!.error.subscribe { error in
            XCTAssertEqual(error.element,LocalizedString.noNewData.value)
        }
    }

    func testPerformanceExample() {
        // This is an example of a performance test case.
        self.measure {
            // Put the code you want to measure the time of here.
        }
    }
}
