//
//  DeliveryServicesTest.swift
//  MVVMAppTests
//
//  Created by varun bhargava on 01/05/19.
//  Copyright © 2019 Nagarro. All rights reserved.
//

import XCTest

class MockServiceAccess : ServiceAccessProtocol {
    func getDeliveries(pagingParameters: PagingParameters, completion: @escaping (Result<[DeliveryServiceEntity], Error>) -> Void) {
        let locationData = Location(address: "Test address", lat: 10, lng: 20)
        let deliveryServiceEntity = DeliveryServiceEntity(id: 2, description: "Test Description", imageUrl: "https://", location: locationData)
        let result = Result<[DeliveryServiceEntity], Error> { return [deliveryServiceEntity]}
        completion(result)
    }
}

class DeliveryServicesTest: XCTestCase {
    
    var deliveryServices:DeliveryServices?

    override func setUp() {
        // Put setup code here. This method is called before the invocation of each test method in the class.
        deliveryServices = DeliveryServices(MockServiceAccess())
    }

    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
    }
    
    func testGetDeliveryList() {
        var deliveryModelList : [DeliveryListModel] = []
        deliveryServices!.getDeliveryList(PagingParameters(offset: 1, limit: 20)) { (result,error)  in
            deliveryModelList.append(contentsOf: result!)
        }
        XCTAssertTrue(deliveryModelList.count==1)
        let testDeliveryModel = deliveryModelList[0]
        XCTAssertEqual(testDeliveryModel.id, 2)
        XCTAssertEqual(testDeliveryModel.description, "Test Description")
        XCTAssertEqual(testDeliveryModel.imageUrl, "https://")
    }
}
